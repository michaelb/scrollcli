#!/bin/bash

NODE_VERSION='v5'
node --version | grep ${NODE_VERSION}
if [[ $? != 0 ]]; then
    echo "ERROR: Expected node v5.x, instead of `node --version`"
    exit 1
fi


########################
# CD into checkout dir
MY_PATH="`dirname \"$0\"`"
MY_PATH="`( cd \"$MY_PATH\" && pwd )`"
cd $MY_PATH
########################

# Add BATS to path
export PATH="$MY_PATH/tests/helpers/bats/bin:$PATH"
########################

# die on err
set -e

bats_cli_all () {
    bats ./tests/*.sh
}

bats_cli_all
