'use strict';
// Generate syntax errors for invalid versions of node
const requires_node_5 = () => {};
const {requires_harmony_destructuring} = {};
const requires_harmony_default_parameters = (param = true) => {};

const path = require('path');
const prettyjson = require('prettyjson');

// Include Scroll deps
const ScrollWorkspace = require('libscroll/mods/workspace/ScrollWorkspace');

function exit_error(message) {
    console.warn(message);
    process.exit(1);
}

function exit_usage() {
    exit_error('Use like: scroll path/to/file action');
}

function print_output(data) {
    // Generic output that changes the form depending on type of data
    if (typeof data === 'object') {
        console.log(prettyjson.render(data));
    } else {
        console.log(data);
    }
}

function split_args(args) {
    args.shift(); // get rid of first argument
    if (args[0].match(/scrollcli.js$/)) { args.shift(); }
    const target = args.length > 0 ? args[0] : null;
    const action = args.length > 1 ? args[1] : null;
    const action_args = args.length > 2 ? args.slice(2) : [];
    return {target, action, action_args};
}

function check_args(split) {
    if (!split.target) {
        exit_usage();
    }
}

function process_args(split, callback) {
    const {target, action, action_args} = split;

    // Check if target is actually a path, or else just use CWD
    const working_dir = (target.match(/\//) || target.match(/^./))
        ? path.resolve(target)
        : process.cwd();

    // Now lets get the workspace from parent dir
    ScrollWorkspace.find_parent_workspace(working_dir, workspace_dir => {
        if (workspace_dir === null) {
            exit_error('Could not find a valid Scroll workspace.');
            return;
        }

        // Figure out the ObjectMatcher (e.g. prefix paths with 'path:')
        const matcher = (target.match(/\//) || target.match(/^\./))
            ? `path:${path.relative(workspace_dir, target)}`
            : target;

        // Load workspace:
        ScrollWorkspace.load(workspace_dir, workspace => {
            const object = workspace.objects.get(matcher);
            if (object === null) {
                exit_error(`Could not find ${matcher}`);
                return;
            }

            if (!action) {
                // Undefined action name, do default describe action
                print_output(workspace.actions.describe(object));
                return;
            }

            // Otherwise, callback with the found object and info
            callback({object, action, action_args});
        });
    });

}

function do_action(action_info) {
    const {object, action, action_args} = action_info;
    // Check if the action makes sense
    if (!(action in object.actions)) {
        exit_error(`${object.typename} does not have ${action}`);
        return;
    }

    // Do the actual action, and echo any output to stdout
    const output = object.actions[action](...action_args);
    if (output) {
        print_output(output);
    }
}

function main(args) {
    const split = split_args(Array.from(args));
    check_args(split);
    process_args(split, action_info => {
        do_action(action_info);
    });
}

main(process.argv);
